package cl.colmena.dapc.tools.valida_caracteres_sp;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.google.common.base.CharMatcher;
import com.google.common.io.Files;

public class Main {

	public static void main(String[] args) throws IOException {
		String dir=(args!=null && args.length>0)?args[0]:"";
		
		File root= new File(dir);
		
		if(!root.isDirectory()) {
			System.err.println("La ruta ["+root+"] no es un directorio");
		}else {
			for(File file:root.listFiles()) {
				if(file.isFile()) {
					boolean archivoConError=false;
					
					List<String> lines = Files.readLines(file, StandardCharsets.UTF_8);
					
					boolean lastTerminaTransaccion=false;
					int ultimaLineaEndSinGO=-1;
					
					for(  int i=0; i < lines.size(); i ++) {
						String line=lines.get(i);
						
						//INI: valida nombre del sp
						String[] nombreSp=line.split(" ");
						if(nombreSp.length==3) {
							if("CREATE".equals(nombreSp[0].toUpperCase().trim()) && "PROCEDURE".equals(nombreSp[1].toUpperCase().trim())) {
								String nombreArchivoSinExtension=file.getName().substring(0, file.getName().length()-4);
								
								if(!nombreArchivoSinExtension.equals(nombreSp[2].trim())){
									archivoConError=true;
									System.err.println("Error en archivo["+file.getName()+"]. el nombre del sp ["+nombreSp[2].trim()+"]no coincide con el nombre del archivo.");									
								}
							}
						}
						//FIN: valida nombre del sp
						
						//INI:valida END GO
						if(lastTerminaTransaccion) {
							lastTerminaTransaccion=false;
							if(!"GO".equals(line.toUpperCase().trim())) {
								ultimaLineaEndSinGO=(i+1);
							}else {
								ultimaLineaEndSinGO=-1;
							}
						}else if("END".equals(line.toUpperCase().trim())){
							lastTerminaTransaccion=true;
						}
						//INI:valida END GO
						
						//valida caracteres
						if(!CharMatcher.ASCII.matchesAllOf(line)) {
							System.err.println("Error en archivo["+file.getName()+"]. caracter especial en linea["+(i+1)+"]");
							archivoConError=true;
						}
					}
					
					if(ultimaLineaEndSinGO!=-1) {
						archivoConError=true;
						System.err.println("Error en archivo["+file.getName()+"] se esperaba un GO en la linea["+ultimaLineaEndSinGO+"]. porque la linea anterior era END");
					}
					
					if(archivoConError) {
						System.err.println("FIN validacion. Error en archivo["+file.getName()+"] finalizado con errores\n\n");
					}else {
						System.out.println("FIN validacion. Error en archivo["+file.getName()+"] finalizado correctamente\n\n");
					}
				}
			}
		}
		System.out.println("Fin proceso");

	}

}
